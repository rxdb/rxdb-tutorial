## RxDB Tutorial (Typescript)


Install `RxDB`

    npm i -S rxdb rxjs

Install at least one data store implementation/adapter. For example,

    npm i -S memdown pouchdb-adapter-leveldb

Or,

    npm i -S pouchdb-adapter-memory


Set tsconfig `compilerOptions.target` to "es5".

Note that the current version (2.6.x) of typescript is compliant with ES2016,
and you can use ES2106 features in Typescript regardless of compiler target.
You may still need polyfill depending on your runtime environment.

    npm i -S babel-polyfill





_tbd_


## References

* [RxDB Doc](https://pubkey.github.io/rxdb/)
* [pubkey/rxdb](https://github.com/pubkey/rxdb)
* []()
* []()
* []()
* []()
