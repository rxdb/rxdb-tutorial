// import 'babel-polyfill';
import * as RxDB from 'rxdb';
import { RxDatabase, RxJsonSchema } from 'rxdb';
import * as memdown from 'memdown';


export class RxdbTsDemo {

  private static schema: RxJsonSchema = {
    title: 'test schema',
    version: 0,
    type: 'object',
    properties: {
      name: {
        type: "string",
        primary: true
      },
      color: {
        type: "string"
      },
    }
  };

  constructor() {
    console.log('RxdbTsDemo loaded....');

    RxDB.plugin(require('pouchdb-adapter-leveldb'));
    RxDB.plugin(require('pouchdb-adapter-memory'));
  }

  private db: RxDatabase;
  // private db: any;

  private async createDB() {
    // this.db = await RxDB.create({
    //   name: 'rxdbtsdemodb',
    //   adapter: 'memory'
    // })
    this.db = await RxDB.create({
      name: 'rxdbtsdemodb',
      adapter: memdown
    })
  }

  private async createCollection() {
    if (this.db) {
      await this.db.collection({
        name: 'tsdemo',
        schema: RxdbTsDemo.schema
      })
    }
  }

  private async tryInsert() {
    if(this.db) {
      this.db['tsdemo'].insert({name: 'Jo', color: 'blue'});
    }
  }

  private async tryDump() {
    if(this.db) {
      let d = await this.db.dump();
      console.log(`dump = ${JSON.stringify(d)}`);
    }
  }

  async testRxDB() {
    console.log('...');

    await this.createDB();
    await this.createCollection();
    await this.tryInsert();
    await this.tryDump();
  }

}
