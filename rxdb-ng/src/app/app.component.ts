import { Component, OnInit } from '@angular/core';
// import 'babel-polyfill';
import * as RxDB from 'rxdb';
import { RxDatabase, RxJsonSchema } from 'rxdb';
// import * as memdown from 'memdown';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private static schema: RxJsonSchema = {
    title: 'test schema',
    version: 0,
    type: 'object',
    properties: {
      name: {
        type: "string",
        primary: true
      },
      color: {
        type: "string"
      },
    }
  };

  title = 'app';
  constructor() {
    console.log('AppComponent loaded....');

    RxDB.plugin(require('pouchdb-adapter-memory'));

    // This throws Webpack error during compile and Javascript error at runtime...
    // RxDB.plugin(require('pouchdb-adapter-leveldb'));
  }

  async ngOnInit() {
    await this.testRxDB();
  }


  private db: RxDatabase;
  // private db: any;

  private async createDB() {
    this.db = await RxDB.create({
      name: 'rxdbtsdemodb',
      adapter: 'memory'
    })
    // this.db = await RxDB.create({
    //   name: 'rxdbtsdemodb',
    //   adapter: memdown
    // })
  }

  private async createCollection() {
    if (this.db) {
      await this.db.collection({
        name: 'tsdemo',
        schema: AppComponent.schema
      })
    }
  }

  private async tryInsert() {
    if (this.db) {
      this.db['tsdemo'].insert({ name: 'Jo', color: 'blue' });
    }
  }

  private async tryDump() {
    if (this.db) {
      let d = await this.db.dump();
      console.log(`dump = ${JSON.stringify(d)}`);
    }
  }

  async testRxDB() {
    console.log('...');

    await this.createDB();
    await this.createCollection();
    await this.tryInsert();
    await this.tryDump();
  }

}
